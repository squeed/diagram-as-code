---
title: Diagrams as code
event: Squeed Brew Tech 2025
author: Andreas Jonsson
date: 2025-01-20
theme:
  name: terminal-dark
  override:
    default:
      margin:
        percent: 1
---

ASCII art pause
===

```text +no_background
  ██████   █████   █    ██ ▓█████ ▓█████ ▓█████▄ 
▒██    ▒ ▒██▓  ██▒ ██  ▓██▒▓█   ▀ ▓█   ▀ ▒██▀ ██▌
░ ▓██▄   ▒██▒  ██░▓██  ▒██░▒███   ▒███   ░██   █▌
  ▒   ██▒░██  █▀ ░▓▓█  ░██░▒▓█  ▄ ▒▓█  ▄ ░▓█▄   ▌
▒██████▒▒░▒███▒█▄ ▒▒█████▓ ░▒████▒░▒████▒░▒████▓ 
▒ ▒▓▒ ▒ ░░░ ▒▒░ ▒ ░▒▓▒ ▒ ▒ ░░ ▒░ ░░░ ▒░ ░ ▒▒▓  ▒ 
░ ░▒  ░ ░ ░ ▒░  ░ ░░▒░ ░ ░  ░ ░  ░ ░ ░  ░ ░ ▒  ▒ 
░  ░  ░     ░   ░  ░░░ ░ ░    ░      ░    ░ ░  ░ 
      ░      ░       ░        ░  ░   ░  ░   ░    
                                          ░      
```

<!-- end_slide -->

<!-- jump_to_middle -->
Diagrams as code?
===

<!-- end_slide -->

Flow charting
===

![](images/gaphor.png)

<!-- end_slide -->

Example
===

<!-- column_layout: [1, 1] -->

<!-- column: 0 -->

```file +no_background
path: diagrams/state.puml
language: plantuml
```
<!-- column: 1 -->
<!-- pause -->
![](diagrams/out/state.png)

<!-- reset_layout -->

<!-- end_slide -->

Tools
===

<!-- pause -->
## Mermaid
<!-- pause -->
## Graphviz (dot)
<!-- pause -->
## PGF/TikZ
<!-- pause -->
## PlantUML

<!-- end_slide -->

```text +no_background
 ███████████  ████                        █████    █████  █████ ██████   ██████ █████      
░░███░░░░░███░░███                       ░░███    ░░███  ░░███ ░░██████ ██████ ░░███       
 ░███    ░███ ░███   ██████   ████████   ███████   ░███   ░███  ░███░█████░███  ░███       
 ░██████████  ░███  ░░░░░███ ░░███░░███ ░░░███░    ░███   ░███  ░███░░███ ░███  ░███       
 ░███░░░░░░   ░███   ███████  ░███ ░███   ░███     ░███   ░███  ░███ ░░░  ░███  ░███       
 ░███         ░███  ███░░███  ░███ ░███   ░███ ███ ░███   ░███  ░███      ░███  ░███      █
 █████        █████░░████████ ████ █████  ░░█████  ░░████████   █████     █████ ███████████
░░░░░        ░░░░░  ░░░░░░░░ ░░░░ ░░░░░    ░░░░░    ░░░░░░░░   ░░░░░     ░░░░░ ░░░░░░░░░░░ 
```

> PlantUML is a highly versatile tool that facilitates the rapid and straightforward creation of a wide array of diagrams.

[](https://plantuml.com)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers
path: diagrams/use-case_1.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_1.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3}
path: diagrams/use-case_2.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_2.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {5-7}
path: diagrams/use-case_3.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_3.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {5,9}
path: diagrams/use-case_4.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_4.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {11-13}
path: diagrams/use-case_5.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_5.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {7,13}
path: diagrams/use-case_6.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_6.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3}
path: diagrams/use-case_6A.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_6A.png)

<!-- end_slide -->

Use case
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {6,21}
path: diagrams/use-case_7.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case_7.png)

<!-- end_slide -->

Use case (final)
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {14,22-25}
path: diagrams/use-case.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/use-case.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3-4}
path: diagrams/activity_1.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_1.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {4-7,9}
path: diagrams/activity_2.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_2.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {8-11,13}
path: diagrams/activity_2A.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_2A.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {12-14}
path: diagrams/activity_3.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_3.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {12,14,16}
path: diagrams/activity_4.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_4.png)

<!-- end_slide -->

Activity
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {13,15}
path: diagrams/activity_5.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity_5.png)

<!-- end_slide -->

Activity (final)
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {17-18}
path: diagrams/activity.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/activity.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3-8}
path: diagrams/sequence_1.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_1.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {4,10}
path: diagrams/sequence_1A.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_1A.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {12-17}
path: diagrams/sequence_2.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_2.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {18-19}
path: diagrams/sequence_2A.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_2A.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {15-16,18,24}
path: diagrams/sequence_3.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_3.png)

<!-- end_slide -->

Sequence
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {13,21,23,25,26,29}
path: diagrams/sequence_4.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence_4.png)

<!-- end_slide -->

Sequence (final)
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3-5}
path: diagrams/sequence.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/sequence.png)

<!-- end_slide -->

Entity-Relationship
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3-33}
path: diagrams/er_1.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/er_1.png)

<!-- end_slide -->

Entity-Relationship
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {35-38}
path: diagrams/er_2.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/er_2.png)

<!-- end_slide -->

Entity-Relationship
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers {3-5}
path: diagrams/er.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/er.png)

<!-- end_slide -->

Class
===

<!-- column_layout: [2, 3] -->

<!-- column: 0 -->

```file +no_background +line_numbers
path: diagrams/class.puml
language: plantuml
```
<!-- column: 1 -->

![](diagrams/out/class.png)

<!-- end_slide -->

Easter Egg
===

```text +no_background
 ███████  ████████  ████████  ██████    ███████  ██    ██ 
██     ██ ██     ██ ██       ██    ██  ██     ██ ███   ██ 
██     ██ ██     ██ ██       ██        ██     ██ ████  ██ 
██     ██ ████████  ██████   ██   ████ ██     ██ ██ ██ ██ 
██     ██ ██   ██   ██       ██    ██  ██     ██ ██  ████ 
██     ██ ██    ██  ██       ██    ██  ██     ██ ██   ███ 
 ███████  ██     ██ ████████  ██████    ███████  ██    ██ 
████████ ████████     ███    ████ ██       
   ██    ██     ██   ██ ██    ██  ██       
   ██    ██     ██  ██   ██   ██  ██       
   ██    ████████  ██     ██  ██  ██       
   ██    ██   ██   █████████  ██  ██       
   ██    ██    ██  ██     ██  ██  ██       
   ██    ██     ██ ██     ██ ████ ████████ 

```

<!-- end_slide -->

C4 Model
===

Simon Brown  

> Ask somebody in the building industry to visually communicate the architecture of a building and you’ll be presented with site plans, floor plans, elevation views, cross-section views and detail drawings. In contrast, ask a software developer to communicate the software architecture of a software system using diagrams and you’ll likely get a confused mess of boxes and lines … 

[](https://c4model.com)

<!-- jump_to_middle -->

# Context
## Containers
### Components
#### Code

<!-- end_slide -->

<!-- jump_to_middle -->
Gantt (extra)
===

<!-- end_slide -->

The End
===

```text +no_background
                                      
                                      
      █▀▀▀▀▀█ ▄ █ ▀▀▄▄▄ ▀▀█ █▀▀▀▀▀█    
      █ ███ █ ▄▄▄ █▄█▄▄▄  █ █ ███ █    
      █ ▀▀▀ █ ▄█ ▄▄▄ ▄█ ▀▀█ █ ▀▀▀ █    
      ▀▀▀▀▀▀▀ ▀▄█▄▀ █▄▀ ▀ ▀ ▀▀▀▀▀▀▀    
      █▀▀▀█▄▀▀▀▀▄▀▄█▀▀▄▀▄▄██▄█▄▀ ▀▄    
      ▄█ ▄▄█▀█▄ ▀ ▄▀███ ▄▄▀▀ ▄▀▄ ▄     
      ▄█  █▀▀  ▀▀ ▀▀▀  ██▄█▄▄▄▄▀▀ ▄    
      ██▀▀  ▀█ ███▀ ▀██▀ ▄██▄▄█▀▀▄     
      ▄▄█▄█▀▀▀█▄ ▀▄▀▀ ▄▀▄█ █▄█▄▀█ ▄    
      █ ▀ █▄▀▄ ▀▄ ▄ ██▀ ▄ █▄█▀▀ ▀▄     
      ▀   ▀▀▀ ▄▄  ▀▄ ▀▄█▄▀█▀▀▀█▄███    
      █▀▀▀▀▀█ ▀▀██▀▀ ▀  ▀██ ▀ █▀▀▄▄    
      █ ███ █ █▄▄▀▄▄ ▀█▀▄▄█▀▀▀▀▄███    
      █ ▀▀▀ █ ██▀ ▄█▀▀█ ▀▀██▀█▀█▀█     
      ▀▀▀▀▀▀▀ ▀   ▀ ▀▀ ▀ ▀  ▀▀▀ ▀      
                                      
                                      
https://gitlab.com/squeed/diagram-as-code
```
