all:
	plantuml -tpng -o out -progress -theme blueprint \
	-Sbackgroundcolor=transparent \
	-Sdpi=150 \
	-SdefaultFontName="Maple Mono" \
	diagrams/*.puml

clean:
	rm -vR ./diagrams/out
